/**
 * Задание 2.
 *
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */
/* Решение */
/* Пример */
console.log(capitalizeAndDoublify('hello')); // HHEELLLLOO
console.log(capitalizeAndDoublify('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!


решение

//
// function capitalizeAndDoublify (str) {
//     str = str.toUpperCase();
//     let result = '';
//     for (let char of str) {
//         result = result + char.repeat(2);
//     }
//     return result;
// }