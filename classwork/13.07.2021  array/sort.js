пример №1

let array = [12,3,2,19,1,77,11];

array.sort();

console.log(array);

пример №2

const heroes = [{
    name: "Bilbo",
    age: 50,
    alive: true,
    gold: 9
}, {
    name: "Nazgul",
    age: 1200,
    alive: false,
    gold: 15
}, {
    name: "Gandalf",
    age: 7777,
    alive: false,
    gold: 50
}, {
    name: "Aragorn",
    age: 86,
    alive: false,
    gold: 59
}, {
    name: 'Galadriel',
    age: 8000,
    alive: true,
    gold: 71

}];


heroes.sort((a , b) => {
    return a.gold - b.gold
});



