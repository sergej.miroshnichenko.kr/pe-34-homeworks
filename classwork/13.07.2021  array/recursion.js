function power(x, n){
    if (n === 1){
        return x;
    }else {
        const result = power(x, n-1);

        return x * result;
    }
}

console.log(power(2,10));