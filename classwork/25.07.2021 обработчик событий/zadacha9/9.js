
// Кликни на элемент списка, чтобы выделить его.



const ul = document.getElementById('ul');
ul.addEventListener('click', event => {
    if (event.target.tagName === 'LI') {
        if ((window.navigator.platform.indexOf('Win') !== -1 && event.ctrlKey)
            || (window.navigator.platform.indexOf('Mac') !== -1 && event.cmdKey)) {
            event.target.classList.toggle('selected');
        } else {
            Array.from(event.target.parentElement.children).forEach(li => {
                li.classList.remove('selected');
            });
            event.target.classList.add('selected');
        }
        return false;
    }
});