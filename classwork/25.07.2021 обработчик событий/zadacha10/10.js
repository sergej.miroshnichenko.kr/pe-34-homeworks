/**
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 */
const enterCounter = document.getElementById('enter-counter');
const spaceCounter = document.getElementById('space-counter');
const backspaceCounter = document.getElementById('backspace-counter');
document.addEventListener('keydown', event => {
    switch (event.code) {
        case 'Enter':
            enterCounter.innerText++;
            break;
        case 'Space':
            spaceCounter.innerText++;
            break;
        case 'Backspace':
            backspaceCounter.innerText++;
            break;
    }
});


