/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

/* Решение */

// 1. Создать h1
// 2. Добавить ему текст
// 3. Добавить его на страницу
// 4. Создать button
// 5. Добавить ему текст
// 6. Добавить его на страницу
// 7. Добавить кнопке обработчик события
// 8. В обработчике :
// 8.1. Получить тескт из элемента h1
// 8.2. Используя for of , для каждой буквы назначить цвет возвращаемый getRandomColor()
// 8.3.1. Для каждой буквы создань элемент span

const h1 = document.createElement('h1');
h1.innerText = PHRASE;
document.body.appendChild(h1);

const button = document.createElement('button');
button.innerText = 'Раскрасить';
document.body.append(button);

button.addEventListener('click', () => {
    const text = h1.innerText;
    h1.innerText = '';

    for (let char of text){
        const span = document.createElement('span');

        span.innerText = char;
        span.style.color = getRandomColor();
        h1.append(span);
    }
});


// char - это буква каждая во фразе
