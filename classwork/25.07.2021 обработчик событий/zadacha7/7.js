/**
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 **/


const input = document.getElementById('new-good-input');
const addButton = document.getElementById('add-btn');
const list = document.querySelector('.shopping-list');

addButton.addEventListener('click', () => {
    if (input.value !== '') {
        const newGood = input.value;
        const li = document.createElement('li');
        const x = document.createElement('button');


        x.innerText = 'X';
        x.classList.add('delete-item');
        li.innerText = newGood;
        li.append(x);
        list.append(li);
        input.value = '';
    }
});

list.addEventListener('click', event => {
    if (event.target.classList.contains('delete-item')){
        event.target.parentElement.remove();
    }
});