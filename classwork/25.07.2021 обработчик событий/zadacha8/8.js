const square = document.querySelector('.square');
const step = 10;
let positionLeft = 0;
let positionTop = 0;
document.addEventListener('keydown', event => {
    if (event.key === 'ArrowRight') {
        positionLeft += step;
        square.style.left = positionLeft + 'px';
    }
    if (event.key === 'ArrowLeft') {
        if (positionLeft > 0) {
            positionLeft -= step;
            square.style.left = positionLeft + 'px';
        }
    }
    if (event.key === 'ArrowUp') {
        if (positionTop > 0) {
            positionTop -= step;
            square.style.top = positionTop + 'px';
        }
    }
    if (event.key === 'ArrowDown') {
        positionTop += step;
        square.style.top = positionTop + 'px';
    }
});