/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */


const p = document.getElementById('counter');
const minus = document.getElementById('decrement-btn');
const plus = document.getElementById('increment-btn');
plus.addEventListener('click', () => {
    let array = p.innerText.split(' ');
    let text = array[0];
    let counter = array[1];
    counter++;
    p.innerText = text + ' ' + counter;
});
minus.addEventListener('click', () => {
    let [text, counter] = p.innerText.split(' ');
    if (counter > 0) {
        counter--;
        p.innerText = `${text} ${counter}`;
    }
});