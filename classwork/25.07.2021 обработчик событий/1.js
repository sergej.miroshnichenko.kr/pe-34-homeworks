// Добавьте JavaScript к кнопке button, чтобы при нажатии элемент <div id="text"> исчезал. Ниже пошагово описал
//
// 1. Получить элемент id=hidebutton
// 2. Назначить обработчик события  click на hidebutton
// 3. В обработчике получить элемент id=text
// 4. Спрятать элемент id=text, например через стиль display none.
// 5. Сделать так чтобы по нажатию кнопки id=text текст прятался/появлялся

const hidebutton = document.getElementById('hidebutton')

const text = document.getElementById('text');

hidebutton.addEventListener('click', () => {

    text.classList.toggle('hidden');
});