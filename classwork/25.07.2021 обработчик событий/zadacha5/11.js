/**
 * Початкове значення кнопки повинно дорівнювати 0
 * При натисканні на кнопку збільшувати це значення на 1
 *
 */

const button = document.querySelector('.counter');

const increment = (event) => {
    let value = event.target.innerText;

    value ++; //value = +value+1
    event.target.innerText = value;
}

button.addEventListener('click', increment);