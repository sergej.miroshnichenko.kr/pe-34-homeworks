// let student = {
//     name : "Sergej",
//     lastName : "Miroshnichenko",
//     laziness : 4,
//     trick : 2
// }


// #2
// если коэффициент лени больше или равен 3, и при этом меньше или равен 5, а коэффициент хитрости меньше или равен 4 - вывести в консоль сообщение Cтудент имяСтудента фамилияСтудента отправлен на пересдачу

// if (student.laziness >=3
//     && student.laziness <=5
//     && student.trick <=4){
//     console.log(`Cтудент ${student.name} ${student.lastName} отправлен на пересдачу`)
// }

// #5
// Создать объект danItStudent, у которого будут такие свойства: имя, фамилия, количество сданных домашних работ. Спросить у пользователя "Что вы хотите узнать о студенте?" и вывести в консоль эту информацию.


// let danItStudent = {
//     name : 'Sergej',
//     lastName: 'Miroshnichenko',
//     homework : 10
// }
//
// let prop = prompt('Что вы хотите узнать о студенте');
// console.log(danItStudent[prop])

//
//
// let danItStudent = {
//      name : 'Sergej',
//     lastName: 'Miroshnichenko',
//     homework : 10
//  }
//
//  let prop = prompt('Что вы хотите узнать о студенте');
//
//  let newValue = prompt('На какое значение')
//
// // if (prop in danItStudent){
// //     danItStudent[prop] = newValue;
// // }     ниже вариант проще как эту строку записать
//
//
// if (danItStudent.hasOwnProperty(prop)){
//     danItStudent[prop] = newValue;
// }
//
//
// console.log(danItStudent);

// /**
//  * Задание 4.
//  *
//  * С помощью цикла for...in вывести в консоль все свойства
//  * первого уровня объекта в формате «ключ-значение».
//  *
//  * Продвинутая сложность:
//  * Улучшить цикл так, чтобы он умел выводить свойства объекта второго уровня вложенности.
//  */
// /* Дано */


 const user = {
    firstName: 'Walter',
    lastName: 'White',
    job: 'Programmer',
    pets: {
        cat: 'Kitty',
        dog: 'Doggy',
    },
};


// Решение

for (let key in user){
    if (typeof user[key] === 'object'){
        for (let key2 in user[key]){
            console.log(user[key][key2]);
        }
    }else {
        console.log(user[key]);
    }
}



