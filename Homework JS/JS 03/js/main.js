let numFirst = +prompt('Введите первое число');
let numSecond = +prompt('Введите второе число');
let operator = prompt('Введите знак операции');

function calcResult(numFirst, numSecond, operator) {
    switch (operator) {
        case "+" :
            return numFirst + numSecond;
        case "-" :
            return numFirst - numSecond;
        case "*" :
            return numFirst * numSecond;
        case "/" :
            return numFirst / numSecond;
    }
}

console.log(calcResult(numFirst, numSecond, operator))


// Теоретический вопрос

// 1)   Описать своими словами для чего вообще нужны функции в программировании.

// Функции нам нужны тогда когда идет повторение одного и того же действия во многих частях программы. Но при помощи функции мы не повторяем один и тот же код во многих местах. Для объявления функции мы используем слово function , после этого имя функции далее список параметров в круглых скобках через запятую и код функции (тело). Основное предназначение функции - это избавление от дублирования кода.

    // 2)   Описать своими словами, зачем в функцию передавать аргумент.

// Аргументы функции - это реальные значения передаваемые и получаемые функцией. При вызове функции ей могут передаваться значения которыми будут инициализированы параметры. Значения которые передаються при вызове функции называються аргументами. Они указываются через запятую. Когда при вызове функции ей передаеться список аргументов они присваиваются параметрам функции в том порядке в каком они указаны - первый аргумент присваивается первому параметру. Если при вызове функции ей передается больше аргументов , чем задано параметров - то лишние аргументы не присваиваются. Если функия имеет больше параметров чем задано ей аргументов то параметрам без соответствующиъ аргументов присваивается значение Undefined.